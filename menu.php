<nav id="menu">
	<ul>
		<a href="index.php"><li class="active">Home</li></a>
		<a href="creations.php"><li class="submenuTrigger">Creations</li></a>
		<a href="creators.php"><li>Creators</li></a>
		<a href="profile.php"><li>Profile</li></a>
	</ul>
	<div id="submenu">
		<img src="img/tri.png"/>
		<ul>
			<a href="creators/aplikasi.php"><li>Aplikasi</li></a>
			<a href="creators/game.php"><li>Game</li></a>
			<a href="creators/video.php"><li>Video</li></a>
			<a href="creators/musik.php"><li>Musik</li></a>
			<a href="creators/lainnya.php"><li>Lainnya</li></a>
		</ul>
	</div>
</nav>