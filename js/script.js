


var active = 1;

window.onload = auto;

function change(next){

	if (next == 0){
		next = active + 1;
		if (next > 5){
			next = 1;
		}
	}
	document.getElementById("slideImg" + next).className += " active";
	document.getElementById("slideImg" + active).className = "slideImg";
	document.getElementById("slide" + next).className = "active";
	document.getElementById("slide" + active).className = "";
	document.getElementById("slideInfo" + next).className = "active";
	document.getElementById("slideInfo" + active).className = "";
	active = next;
}

function auto(){
	setInterval(function() { change(0) },4000);
}

function showLoginBox(){
	if (document.getElementById("containerLoginBox").className == "show"){
		document.getElementById("containerLoginBox").className = "";
	}
	else{
		document.getElementById("containerLoginBox").className = "show";
	}
}


function validate(){
	var isnum = /^\d+$/.test(document.getElementById("nimm").value);

     if(!isnum){
     	document.getElementById("nimm").className = "warning";
     	document.getElementById("validator").className = "show";
     }
     else{
     	document.getElementById("nimm").className = "";
     	document.getElementById("validator").className = "";
     }
}


$(document).ready(function() {

	// $(document).on('click', 'div', function() { alert("hello"); });

	$("#slideNext").click(function(){
		var next = active + 1;
		if (next > 5){
			next = 1;
		}
		change(next);
	});

	$("#slidePrev").click(function(){
		var next = active - 1;
		if (next < 1){
			next = 5;
		}
		change(next);
	});


	$(".scrollNext").click(function(){
		var set = parseInt($(this).siblings(".scrollable").css("margin-left")) - 200;
		if (set <= -500){
			set = -500;
			$(this).css("opacity", "0");
		}
		$(this).siblings(".scrollable").css("margin-left",set + "px");
		$(this).siblings(".scrollPrev").css("opacity", "1");
	});

	$(".scrollPrev").click(function(){
		var set = parseInt($(this).siblings(".scrollable").css("margin-left")) + 200;
		if (set >= 0){
			set = 0;
			$(this).css("opacity", "0");
		}
		$(this).siblings(".scrollable").css("margin-left",set + "px");
		$(this).siblings(".scrollNext").css("opacity", "1");
	});

	$(".submenuTrigger").hover(function(){
		$("#submenu").addClass("show");
		$(".submenuTrigger").addClass("hovered");
	},
	function(){
		$("#submenu").removeClass("show");
		$(".submenuTrigger").removeClass("hovered");
	}
	);

	$("#submenu").hover(function(){
		$("#submenu").addClass("show");
		$(".submenuTrigger").addClass("hovered");
	},
	function(){
		$("#submenu").removeClass("show");
		$(".submenuTrigger").removeClass("hovered");
	}
	);

	$(document).scroll(function() {
		if ($(this).scrollTop()>100 ){
			$("#containerFixedTitle").addClass("show");
		}
		else{
			$("#containerFixedTitle").removeClass("show");
		}
	});

	$("#searchBtn").click(function(){
		$("#containerSearchTitle input").focus();
		$("#containerSearchTitle").addClass("active");
		$(this).addClass("active");
	});

	$("#containerSearchTitle input").blur(function(){
		$("#containerSearchTitle").removeClass("active");
		$("#searchBtn").removeClass("active");
	});


	$(document).keypress(function(e) {
	    if(e.which == 13) {
	        if ($("#containerSearchTitle input").is(':focus')){
	 	       window.location.href = "search.php?query=" + $("#containerSearchTitle input").val();
	        }
	    }
	});



});



// function autoChange(){
// 	var n = active + 1;
// 	if (n > 5){
// 		n = 1;
// 	}
// 	change(n);
// }