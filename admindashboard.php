<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
<?php
include "include.php";
?>
</head>

<body>

<?php
include "header.php";
include "adminheader.php";
?>

<div id="containerBody">

	<article>
	
		<?php
		$mn = 'user';
		if (isset($_GET['menu'])) {
			$mn = $_GET['menu'];
		}

		include "admindashboardmenu.php"
		?>
		
		<?php
			if ($mn == 'user'){
		?>

		<h1 class="borderBot">Admin List</h1>
		
		<table id="registeredUserList">
			<thead>
				<td>No.</td>
				<td>NIM</td>
				<td>Nama</td>
				<td>Password</td>
			</thead>
			<tbody>
				<?php
					include "connect.php";
					$sql = "SELECT user_nim, user_fullname, user_pass, user_id  FROM user WHERE user_admin = '1'";
					$result = mysqli_query($conn, $sql);
					$i = 1;
					while($row = mysqli_fetch_array($result)){
						echo "<tr>
							<td>$i</td>
							<td>$row[user_nim]</td>
							<td><a href='profile.php?id=$row[user_id]'>$row[user_fullname]</a></td>
							<td>$row[user_pass]</td>
						</tr>";
						$i++;
					}
				?>

			</tbody>
		</table>

		<hr>

		<h1 class="borderBot">Registered User List</h1>
		
		<table id="registeredUserList">
			<thead>
				<td>No.</td>
				<td>NIM</td>
				<td>Nama</td>
				<td>Password</td>
			</thead>
			<tbody>
				<?php
					include "connect.php";
					$sql = "SELECT user_nim, user_fullname, user_pass, user_id FROM user WHERE user_admin = '0'";
					$result = mysqli_query($conn, $sql);
					$i = 1;
					while($row = mysqli_fetch_array($result)){
						echo "<tr>
							<td>$i</td>
							<td>$row[user_nim]</td>
							<td><a href='profile.php?id=$row[user_id]'>$row[user_fullname]</a></td>
							<td>$row[user_pass]</td>
						</tr>";
						$i++;
					}
				?>
			</tbody>
		</table>

		<?php
			}

			else if ($mn == 'creation'){
		?>

				<h1 class="borderBot">Remove Creations</h1>
		
				<div class="containerCreations4x">

			<?php
				include "connect.php";
				$sql = "SELECT * FROM creation WHERE cr_publish='1' ORDER BY cr_date DESC";
				$result = mysqli_query($conn, $sql);
				while($row = mysqli_fetch_array($result)){
					$sql_user = "SELECT * FROM user WHERE user_id = '$row[cr_creators]'";
					$result_user = mysqli_query($conn, $sql_user);
					$row_user = mysqli_fetch_array($result_user);

					$sql_likes = "SELECT * FROM likes WHERE lk_creation = '$row[cr_id]'";
					$result_likes = mysqli_query($conn, $sql_likes);
					$likes = mysqli_num_rows($result_likes);

					$sql_comment = "SELECT * FROM comment WHERE com_creation = '$row[cr_id]'";
					$result_comment = mysqli_query($conn, $sql_comment);
					$countcomment = mysqli_num_rows($result_comment);

					$sql_rate = "SELECT * FROM rate WHERE rt_creation = '$row[cr_id]'";
					$result_rate = mysqli_query($conn, $sql_rate);
					$countrate = mysqli_num_rows($result_rate);		
					$rate = 0;
					if($countrate != 0){
						while($row_rate = mysqli_fetch_array($result_rate)){
							$rate += $row_rate['rt_score'];
						}
						$rate /= $countrate;
					}
					?>
						<a href="creation.php?id=<?php echo ($row['cr_id']) ?>"/>
						<div style="background-image: url('img/creation/<?php echo ($row['cr_id']) ?>.jpg')">
								<a href="remove.php?id=<?php echo ($row['cr_id']) ?>"><div class="remove"><span class="colorred icon icon-remove"></span></div></a>
								<div class="info">

									
									<h1><?php echo ($row['cr_title']) ?></h1>
									<h2>by: <?php echo ($row_user['user_fullname']) ?></h2>

									<img class="thumbCreators" src="img/usr/<?php echo ($row_user['user_nim']) ?>.jpg"/>

									<div class="parameter">
										<span>
											<span class="icon icon-eye-open"></span> <?php echo ($row['cr_view']) ?> 
										</span>
										<span>
											<span class="icon icon-heart"></span> <?php echo ($likes) ?>  
										</span>
										<span>
											<span class="icon icon-comment"></span> <?php echo ($countcomment) ?>
										</span>
									</div>

									<div class="rate">
										<?php
											$printed = 0;
											for ($i = 1; $i < $rate; $i++){
												echo "<span class='icon icon-star'></span>";
												$printed++;
											}
											for ($i = 0; $i < 5 - $printed; $i++){
												echo "<span class='icon icon-star-empty'></span>";
											}
										?>
									</div>


								</div>
							</div>
					</a>
					<?php
				}

			?>

		</div>

		<?php
			}

			else{
		?>

				<h1 class="borderBot">Pending Creations</h1>
		
				<table id="pendingCreationsList">
					<thead>
						<td>No.</td>
						<td>Title</td>
						<td>User</td>
						<td>Date</td>
						<td>Action</td>
					</thead>
					<tbody>
						<?php
							include "connect.php";
							$sql = "SELECT * FROM creation WHERE cr_publish = '0'";
							$result = mysqli_query($conn, $sql);
							$i = 1;
							while($row = mysqli_fetch_array($result)){
								$sql_creator = "SELECT user_id, user_fullname FROM user WHERE user_id = '$row[cr_creators]'";
								$result_creator = mysqli_query($conn, $sql_creator);
								$row_creator = mysqli_fetch_array($result_creator);
								echo "<tr>
									<td>$i</td>
									<td><a href='creation.php?id=$row[cr_id]'>$row[cr_title]</a></td>
									<td><a href='profile.php?id=$row_creator[user_id]'>$row_creator[user_fullname]</a></td>
									<td>$row[cr_date]</td>
									<td style='width:120px;'>
										<a href='approval.php?action=accept&id=$row[cr_id]'><span class='icon icon-ok-sign colorgreen'></span></a>
										<a href='approval.php?action=deny&id=$row[cr_id]'><span class='icon icon-remove-sign colorred'></span></a>
									</td>

								</tr>";
								$i++;
							}
						?>

					</tbody>
				</table>

		<?php
			}

		?>
		
		

		
		</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>