<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery-2.2.2.js"></script>
	

	<script type="text/javascript" src="js/script.js"></script>

</head>

<body>

<?php
include "header.php"
?>

<div id="containerBody">
	<?php
	include "menu.php"
	?>
	<article>
		
		<h1 class="borderBot">Newest Creations</h1>
		<div class="wrapperCreations4x">
			<div class="scrollNext"><span class="icon icon-chevron-right"></span></div>
			<div class="scrollPrev"><span class="icon icon-chevron-left"></span></div>
			<div class="containerCreations4x scrollable">
				
				<div style="background-image: url('img/angry.jpg')">
					<div class="info">

						
						<h1>Angry Bird</h1>
						<h2>by: Tibi Sejatii</h2>

						<img class="thumbCreators" src="img/tibi.jpg"/>

						<div class="parameter">
							<span>
								<span class="icon icon-eye-open"></span> 145 
							</span>
							<span>
								<span class="icon icon-heart"></span> 145 
							</span>
							<span>
								<span class="icon icon-comment"></span> 145 
							</span>
						</div>

						<div class="rate">
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star-empty"></span>

						</div>


					</div>
				</div>

				<div style="background-image: url('img/cytus.png')">
					<div class="info">

						
						<h1>Cytus</h1>
						<h2>by: Erwinata</h2>

						<img class="thumbCreators" src="img/ew.jpg"/>

						<div class="parameter">
							<span>
								<span class="icon icon-eye-open"></span> 145 
							</span>
							<span>
								<span class="icon icon-heart"></span> 145 
							</span>
							<span>
								<span class="icon icon-comment"></span> 145 
							</span>
						</div>

						<div class="rate">
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star-empty"></span>

						</div>


					</div>
				</div>

				<div style="background-image: url('img/fffxv.jpg')">
					<div class="info">

						
						<h1>Final Fantasy XV</h1>
						<h2>by: Dimas Pangestu</h2>

						<img class="thumbCreators" src="img/dp.jpg"/>

						<div class="parameter">
							<span>
								<span class="icon icon-eye-open"></span> 145 
							</span>
							<span>
								<span class="icon icon-heart"></span> 145 
							</span>
							<span>
								<span class="icon icon-comment"></span> 145 
							</span>
						</div>

						<div class="rate">
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star-empty"></span>

						</div>


					</div>
				</div>

				<div style="background-image: url('img/deadpool-cover.jpg')">
					<div class="info">

						
						<h1>Deadpool</h1>
						<h2>by: Yurian Prabowo</h2>

						<img class="thumbCreators" src="img/tibi.jpg"/>

						<div class="parameter">
							<span>
								<span class="icon icon-eye-open"></span> 145 
							</span>
							<span>
								<span class="icon icon-heart"></span> 145 
							</span>
							<span>
								<span class="icon icon-comment"></span> 145 
							</span>
						</div>

						<div class="rate">
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star-empty"></span>

						</div>


					</div>
				</div>

				<div style="background-image: url('img/batvssup.jpg')">
					<div class="info">

						
						<h1>Batman Vs Superman</h1>
						<h2>by: Tibi Sejatii</h2>

						<img class="thumbCreators" src="img/tibi.jpg"/>

						<div class="parameter">
							<span>
								<span class="icon icon-eye-open"></span> 145 
							</span>
							<span>
								<span class="icon icon-heart"></span> 145 
							</span>
							<span>
								<span class="icon icon-comment"></span> 145 
							</span>
						</div>

						<div class="rate">
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star"></span>
							<span class="icon icon-star-empty"></span>

						</div>


					</div>
				</div>


			</div>
		</div>


		<a href="creations.php"><div class="button more"><span class="icon icon-chevron-right"></span> See More</div></a>

		<hr>

		<h1 class="borderBot">Featured Creations</h1>

		<div class="containerCreationsBasicInfo">
			<img src="img/cytus.png"/>
			<div class="containerInfo">
				<h1>Cytus</h1>
				<h2>Game</h2>
				<h3>Music & Rhythm</h3>
				<div class="creators"><span class="marginRight10">Oleh : </span><img src="img/ew.jpg"><span>erwinata</span></div>
				
				<p>Cytus is a rhythm game developed by Rayark Games, an independent game developer in Taiwan. It was first released on the iOS platform on January 12, 2012. The game was later released on Android on August 7, 2012.
				<br>
				The name Cytus is taken from Cocytus, the Greek mythological river of wailing or lamentation.[citation needed] Cytus features a story plot that is carried through its 10 main chapters as well as a prologue.[4] The story revolves around the protagonist, Vanessa, who lives in a 22nd-century world where robots with human memories are the only remaining sentient beings while living humans are killed by a mutant virus. Due to limited memory space, human emotions are stored in the form of music in "Cytus", and by playing the songs, the robots are able to revive their human emotions. The plot features the disaster and the protagonist's struggle with identity.[4] The story is told in-game through various cut-scenes that are unique to the chapter title songs; in comparison, the other individual songs within each chapter are at most only marginally relevant to the plot, except for a limited few.</p>
			</div>
			<div class="readMoreCreations pointer"><span class="icon icon-chevron-right"></span><span class="text">Read More</span></div>
		</div>

		<hr>

		<h1 class="borderBot">Meet the Creators</h1>
		<div class="containerCreators4x">
			
			<div>
				<img src="img/tibi.jpg">
				<div class="info">

					<h1>Tibi Sejatii</h1>
					<h2>TI 2014</h2>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>


				</div>
			</div>

			<div>
				<img src="img/ew.jpg">
				<div class="info">

					<h1>Erwin Winata</h1>
					<h2>TI 2014</h2>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>


				</div>
			</div>
			<div>
				<img src="img/dp.jpg">
				<div class="info">

					<h1>Dimas Pangestu</h1>
					<h2>TI 2014</h2>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>


				</div>
			</div>

			<div>
				<img src="img/tibi.jpg">
				<div class="info">

					<h1>Tibi Sejatii</h1>
					<h2>TI 2014</h2>

					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</p>


				</div>
			</div>



		</div>

		<a href="creations.php"><div class="button more"><span class="icon icon-chevron-right"></span> See More</div></a>

<!--
		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"/>
			<div class="deskripsi">
				<h1>Angry Birds</h1>
				Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone,
				namun kini telah dirilis di berbagai media.
				Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. 
				Bahkan pejabat Inggris pun tertarik dengan permainan ini.
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		
		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="https://upload.wikimedia.org/wikipedia/id/thumb/3/3a/Piala_Oscar.jpg/220px-Piala_Oscar.jpg"/>
			<div class="deskripsi">
				<h1>Piala Oscar</h1>
				Piala Oscar adalah penghargaan film Amerika untuk menghargai karya dalam industri film.
				Berbagai kategori pemenang diberikan penghargaan berupa sebuah salinan patung oleh Academy Award of Merit, yang lebih dikenal dengan julukan Oscar.
				Penghargaan ini pertama kali diselenggarakan pada tahun 1929 di Hotel Hollywood Roosevelt.
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="http://animalia-life.com/data_images/alaskan-malamute/alaskan-malamute1.jpg"/>
			<div class="deskripsi">
				<h1>Alaskan Malamut</h1>
			Malamut Alaska adalah salah satu ras anjing bertubuh besar yang berasal dari Alaska.
			Dulunya ras anjing ini dimanfaatkan untuk menarik kereta barang yang tahan terhadap suhu sangat dingin,
			namun sekarang anjing ini dirawat sebagai peliharaan keluarga dan anjing pertunjukan.	
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"/>
			<div class="deskripsi">
				<h1>Angry Birds</h1>
				Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone,
				namun kini telah dirilis di berbagai media.
				Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. 
				Bahkan pejabat Inggris pun tertarik dengan permainan ini.
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"/>
			<div class="deskripsi">
				<h1>Angry Birds</h1>
				Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone,
				namun kini telah dirilis di berbagai media.
				Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. 
				Bahkan pejabat Inggris pun tertarik dengan permainan ini.
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		<ul id="containerPage">
			<li id="1" class="active">1</li>
			<li id="2">2</li>
			<li id="3">3</li>
			<li id="4">4</li>
			<li id="5">5</li>
			<li id="5">></li>
		</ul>

-->

	</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>