<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
<?php
include "include.php";
?>
<?php
	include "connect.php";
	if (isset($_GET['id'])){
		$sql = "SELECT * FROM creation WHERE cr_id = '$_GET[id]'";
		$result = mysqli_query($conn, $sql);
		$row = mysqli_fetch_array($result);

		$viewplus = $row['cr_view'] + 1;
		$sql_view = "UPDATE creation SET cr_view = '$viewplus' WHERE cr_id = '$_GET[id]'";
		mysqli_query($conn, $sql_view);

		$sql_cat = "SELECT * FROM category WHERE cat_id = '$row[cr_category]'";
		$result_cat = mysqli_query($conn, $sql_cat);
		$row_cat = mysqli_fetch_array($result_cat);

		$sql_subcat = "SELECT * FROM subcategory WHERE subcat_id = '$row[cr_subcategory]'";
		$result_subcat = mysqli_query($conn, $sql_subcat);
		$row_subcat = mysqli_fetch_array($result_subcat);

		$sql_creators = "SELECT * FROM user WHERE user_id = '$row[cr_creators]'";
		$result_creators = mysqli_query($conn, $sql_creators);
		$row_creators = mysqli_fetch_array($result_creators);

		$sql_likes = "SELECT * FROM likes WHERE lk_creation = '$row[cr_id]'";
		$result_likes = mysqli_query($conn, $sql_likes);
		$likes = mysqli_num_rows($result_likes);

		$sql_comment = "SELECT * FROM comment WHERE com_creation = '$row[cr_id]'";
		$result_comment = mysqli_query($conn, $sql_comment);
		$countcomment = mysqli_num_rows($result_comment);

		$sql_rate = "SELECT * FROM rate WHERE rt_creation = '$row[cr_id]'";
		$result_rate = mysqli_query($conn, $sql_rate);
		$countrate = mysqli_num_rows($result_rate);		
		$rate = 0;
		if($countrate != 0){
			while($row_rate = mysqli_fetch_array($result_rate)){
				$rate += $row_rate['rt_score'];
			}
			$rate /= $countrate;
		}




		
	}
?>
<meta property="og:url" content="http://vhost.ti.ukdw.ac.id/creation.php" />
<meta property="og:image" content="img/cytus.png" />
<meta property="og:title" content="FTI Showcase : CYTUS" />
<meta property="og:description" content="Rhythm game developed by Rayark Games, an independent game developer in Taiwan" />
</head>

<body>

<?php
include "header.php";
include "creationheader.php";
?>

<div id="containerBody">

	<article>
		<div class="containerInfoCategoryCreators">
			<h2><?php echo ($row_cat['cat_category']) ?></h2>
			<h3><?php echo ($row_subcat['subcat_subcategory']) ?></h3>
			<div class="creators"><span class="marginRight10">Oleh : </span><img src="img/usr/<?php echo ($row_creators['user_nim']) ?>.jpg"><a href="profile.php?id=<?php echo ($row_creators['user_id']) ?>"><span><?php echo ($row_creators['user_fullname']) ?></span></a></div>
		</div>
	
		
		<div class="containerParamRateShareDate">
			<?php
				if (isset($_SESSION['id'])){
					if ($_SESSION['id'] == $row_creators['user_id']){
						?>
						<a href="edit.php?id=<?php echo ($row['cr_id']) ?>"><button id="moreDetail">EDIT CREATION</button></a>
						<?php
					}
				}
			?>
			
			<a href="<?php echo ($row['cr_link']) ?>"><button id="moreDetail" class="paramButton">MORE DETAIL</button></a>
			<?php
				if (isset($_SESSION['id'])){
					if ($_SESSION['id'] != $row_creators['user_id']){
						$s = "SELECT lk_id FROM likes WHERE lk_user = '$_SESSION[id]' AND lk_creation = '$_GET[id]'";
						$r = mysqli_query($conn, $s);
						if (mysqli_num_rows($r) == 0){
					
			?>
			<a href="lovethis.php?id=<?php echo ($row['cr_id']) ?>"><button id="loveThis" class="paramButton">LOVE THIS</button></a>
			<?php
						}
					}
				}

				if (isset($_SESSION['id'])){
					if ($_SESSION['id'] != $row_creators['user_id']){
						$s = "SELECT rt_id FROM rate WHERE rt_user = '$_SESSION[id]' AND rt_creation = '$_GET[id]'";
						$r = mysqli_query($conn, $s);
						if (mysqli_num_rows($r) == 0){
			?>
			<div id="rate" class="paramButton">RATE<br>
			<a href="rate.php?rate=1&id=<?php echo ($row['cr_id']) ?>"><span class='icon icon-star'></span></a>
			<a href="rate.php?rate=2&id=<?php echo ($row['cr_id']) ?>"><span class='icon icon-star'></span></a>
			<a href="rate.php?rate=3&id=<?php echo ($row['cr_id']) ?>"><span class='icon icon-star'></span></a>
			<a href="rate.php?rate=4&id=<?php echo ($row['cr_id']) ?>"><span class='icon icon-star'></span></a>
			<a href="rate.php?rate=5&id=<?php echo ($row['cr_id']) ?>"><span class='icon icon-star'></span></a>
			</div>
			<?php
						}
					}
				}
			?>

			<div class="containerParam">
				<span>
					<span class="icon icon-eye-open"></span> <?php echo ($row['cr_view']) ?> 
				</span>
				<span>
					<span class="icon icon-heart"></span> <?php echo ($likes) ?>
				</span>
				<span>
					<span class="icon icon-comment"></span> <?php echo ($countcomment) ?> 
				</span>
			</div>
			<div class="containerRate">
				<h3><?php echo ($rate) ?> <span>/5</span></h3>
				<div class="rate">
					<?php
						for($i = 1; $i<=$rate; $i++)
							echo "<span class='icon icon-star'></span>";
						for($i = 0; $i<5 - $rate; $i++)
							echo "<span class='icon icon-star-empty'></span>";
					?>
				</div>
			</div>
			<div class="containerDate">
				Published on
				<h3><?php echo ($row['cr_date']) ?></h3>
			</div>
		</div>
		<div class="containerDescPhoto">
			<h1>Description</h1>
			<p><?php echo ($row['cr_description']) ?></p>

			<!-- <h1>Images</h1>
			<div class="containerPhoto">
				<img src="img/cytus.png"/>
				<img src="img/cytus.png"/>
				<img src="img/cytus.png"/>
				<img src="img/cytus.png"/>
			</div> -->
		</div>
		
		
		<br/>
		<br/>

		<?php
			include "connect.php";
			$sql = "SELECT * FROM creation WHERE cr_publish = '1' AND cr_category = $row[cr_category] AND cr_id != $row[cr_id] ORDER BY cr_view DESC";
			$result = mysqli_query($conn, $sql);
			$count = mysqli_num_rows($result);

			if ($count > 0) {
				?>

			<h1>Related Creations</h1>

			<div class="containerCreations4x">

			<?php			
					while($row = mysqli_fetch_array($result)){
						$sql_user = "SELECT * FROM user WHERE user_id = '$row[cr_creators]'";
						$result_user = mysqli_query($conn, $sql_user);
						$row_user = mysqli_fetch_array($result_user);

						$sql_likes = "SELECT * FROM likes WHERE lk_creation = '$row[cr_id]'";
						$result_likes = mysqli_query($conn, $sql_likes);
						$likes = mysqli_num_rows($result_likes);

						$sql_comment = "SELECT * FROM comment WHERE com_creation = '$row[cr_id]'";
						$result_comment = mysqli_query($conn, $sql_comment);
						$countcomment = mysqli_num_rows($result_comment);

						$sql_rate = "SELECT * FROM rate WHERE rt_creation = '$row[cr_id]'";
						$result_rate = mysqli_query($conn, $sql_rate);
						$countrate = mysqli_num_rows($result_rate);		
						$rate = 0;
						if($countrate != 0){
							while($row_rate = mysqli_fetch_array($result_rate)){
								$rate += $row_rate['rt_score'];
							}
							$rate /= $countrate;
						}
						?>
							<a href="creation.php?id=<?php echo ($row['cr_id']) ?>">
							<div style="background-image: url('img/creation/<?php echo ($row['cr_id']) ?>.jpg')">
									<div class="info">

										
										<h1><?php echo ($row['cr_title']) ?></h1>
										<h2>by: <?php echo ($row_user['user_fullname']) ?></h2>

										<img class="thumbCreators" src="img/usr/<?php echo ($row_user['user_nim']) ?>.jpg"/>

										<div class="parameter">
											<span>
												<span class="icon icon-eye-open"></span> <?php echo ($row['cr_view']) ?> 
											</span>
											<span>
												<span class="icon icon-heart"></span> <?php echo ($likes) ?>  
											</span>
											<span>
												<span class="icon icon-comment"></span> <?php echo ($countcomment) ?>
											</span>
										</div>

										<div class="rate">
											<?php
												$printed = 0;
												for ($i = 1; $i < $rate; $i++){
													echo "<span class='icon icon-star'></span>";
													$printed++;
												}
												for ($i = 0; $i < 5 - $printed; $i++){
													echo "<span class='icon icon-star-empty'></span>";
												}
											?>
										</div>


									</div>
								</div>
								</a>
						<?php
					}

				?>

			</div>

				<?php
			}
		?>
		

		<?php
			if ($countcomment > 0){
				?>
					<h1>Comments</h1>
				<?php
			}
		?>
		
		<div class="containerComment">
			<?php
				while($row_comment = mysqli_fetch_array($result_comment)){
					$sql_user = "SELECT * FROM user WHERE user_id = '$row_comment[com_user]'";
					$result_user = mysqli_query($conn, $sql_user);
					$row_user = mysqli_fetch_array($result_user);
			?>
					<div class="comment">
						<img src="img/usr/<?php echo ($row_user['user_nim']) ?>.jpg"/>
						<div class="commentContent">
							<h1><?php echo ($row_user['user_fullname']) ?></h1>
							<h3><?php echo ($row_comment['com_date']) ?></h3>
							<p><?php echo ($row_comment['com_comment']) ?></p>
						</div>
					</div>
			<?php
				}
			?>
			
		</div>
		
		</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>