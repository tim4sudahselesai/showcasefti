<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
	<?php
		include "include.php"
	?>
</head>

<body>

<?php
include "header.php"
?>

<div id="containerBody">
	<article>

		<div class="containerDetailed">
			<h1>Angry Birds</h1>
			<img src="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"/>
			<div class="deskripsi">
				Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone,
				namun kini telah dirilis di berbagai media.
				Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. 
				Bahkan pejabat Inggris pun tertarik dengan permainan ini.
			</div>

		</div>


	</article>

</div>

<?php
include "footer.php"
?>

</body>
</html>