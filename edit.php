<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
<?php
include "include.php";
?>
<meta property="og:url" content="http://vhost.ti.ukdw.ac.id/creation.php" />
<meta property="og:image" content="http://vhost.ti.ukdw.ac.id/img/cytus.png" />
<meta property="og:title" content="FTI Showcase : CYTUS" />
<meta property="og:description" content="Rhythm game developed by Rayark Games, an independent game developer in Taiwan" />
</head>

<body>

<?php
include "header.php";
$msg = "Edit Creation";
include "messageheader.php";
?>

<div id="containerBody">

	<article>
	
		<?php
			include "connect.php";
			$sql = "SELECT * FROM creation WHERE cr_id = '$_GET[id]'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

		?>
		
		<h1>Data</h1>
		<form  enctype="multipart/form-data"  action="editprocess.php?id=<?php echo $row['cr_id'] ?>" method="POST">
			 
		<table id="profileTableDataDiri">
			<tbody>
				<tr>
					<td>Title</td>
					<td><input type="text" name="title" value="<?php echo $row['cr_title'] ?>"></input></td>
				</tr>
				<tr>
					<td>Short Description</td>
					<td><input type="text" name="shortdesc" value="<?php echo $row['cr_shortdesc'] ?>"></input></td>
				</tr>
				<tr>
					<td>External Link to this Product</td>
					<td><input type="text" name="link" value="<?php echo $row['cr_link'] ?>"></input></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea name="desc"><?php echo $row['cr_description'] ?></textarea></td>
				</tr>
			</tbody>
		</table>
		
		<button type="submit" class="marginAuto">Submit</button>
		
		</form>
				
		</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>