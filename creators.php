<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
	<?php
		include "include.php"
	?>
</head>

<body>

<?php
include "header.php";
include "creatorsheader.php";
?>

<div id="containerBody">

	<article>
		
		<div class="containerCreators4x">
			
		<?php
			include "connect.php";
			$sql = "SELECT * FROM user WHERE user_admin = 0 ORDER BY user_view DESC";
			$result = mysqli_query($conn, $sql);
			while($row = mysqli_fetch_array($result)){
		?>
			<a href="profile.php?id=<?php echo ($row['user_id']) ?>">
				<div>
					<img src="img/usr/<?php echo ($row['user_nim']) ?>.jpg">
					<div class="readMoreCreations pointer"><span class="icon icon-chevron-right"></span><span class="text">See Profile</span></div>
					<div class="info">

						<h1><?php echo ($row['user_fullname']) ?></h1>
						<h2><?php echo ($row['user_title']) ?></h2>

						<p>
							<?php echo ($row['user_desc']) ?>
						</p>
					</div>
				</div>
			</a>
			
		<?php
			}
		?>
			




		</div>


	</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>