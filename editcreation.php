<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
<?php
include "include.php";
?>
<meta property="og:url" content="http://vhost.ti.ukdw.ac.id/creation.php" />
<meta property="og:image" content="http://vhost.ti.ukdw.ac.id/img/cytus.png" />
<meta property="og:title" content="FTI Showcase : CYTUS" />
<meta property="og:description" content="Rhythm game developed by Rayark Games, an independent game developer in Taiwan" />
</head>

<body>

<?php
include "header.php";
$msg = "Upload Creation";
include "messageheader.php";
?>

<div id="containerBody">

	<article>
	
		<?php
			include "connect.php";
			$sql = "SELECT * FROM user WHERE user_id = '$_SESSION[id]'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

		?>
		
		<h1>Data</h1>
		<form action="editprofileprocess.php" method="POST">
			 
		<table id="profileTableDataDiri">
			<tbody>
				<tr>
					<td>Title</td>
					<td><input type="text" name="title"></input></td>
				</tr>
				<tr>
					<td>Short Description</td>
					<td><input type="text" name="shortdesc"></input></td>
				</tr>
				<tr>
					<td>Icon Photo</td>
					<td><input type="file" name="photo"></input></td>
				</tr>
				<tr>
					<td>Cover Photo</td>
					<td><input type="file" name="photo"></input></td>
				</tr>
				<tr>
					<td>Category</td>
					<td>
						<select>
							<option value="1">Application</option>
							<option value="2">Game</option>
							<option value="3">Music</option>
							<option value="4">Video</option>
							<option value="5">Other</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Subcategory</td>
					<td><select>
							<option value="1">Application</option>
							<option value="2">Game</option>
							<option value="3">Music</option>
							<option value="4">Video</option>
							<option value="5">Other</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>External Link to this Product</td>
					<td><input type="text" name="address" value="<?php echo $row['user_alamat']?>"></input></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea name="desc"><?php echo $row['user_desc']?></textarea></td>
				</tr>
			</tbody>
		</table>
		
		<button type="submit" class="marginAuto">Submit</button>
		
		</form>
				
		</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>