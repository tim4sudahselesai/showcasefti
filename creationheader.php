<div id="containerHead" class="creation">
<div id="containerHeadOverlay">

	<div id="wrapperCreationBasicInfo" style="background-image: url('img/creation/cov/<?php echo ($row['cr_id']) ?>.jpg')">
		<div id="wrapperCreationBasicInfoOverlay" >
			<img src="img/creation/<?php echo ($row['cr_id']) ?>.jpg"/>
			<div id="containerCreationBasicInfo">
				<h1><?php echo ($row['cr_title']) ?></h1>
				<p><?php echo ($row['cr_shortdesc']) ?></p>
			</div>
		</div>
	</div>

</div>
</div>
