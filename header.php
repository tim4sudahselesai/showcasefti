<?php
	session_start();
	if (!isset($sec)){
		$sec = 0;
	}
?>
<div id="containerFixedTitle">
	<div id="containerTitle">
		<a href="index.php"><img src="img/logo2.png" alt="logo" /></a>
		


		<div id="containerLoginBtnTitle">
		<?php
			if (isset($_SESSION['id'])) {
				echo "<a href='logout.php'><div class='button' id='logout'>LOG OUT</div></a>";
			}
			else{
				echo "<a><div class='button' id='login' onclick='showLoginBox()'>LOG IN</div></a>";
			}
		?>
			
			<div class="divider"></div>
		</div>
		<div id="containerBtnTitle">
		<?php
			if (isset($_SESSION['id'])) {
				if ($_SESSION['admin']) {
					echo "<a href='admindashboard.php'><div class='button' id='profile'>ADMIN DASHBOARD</div></a>";
				}
				else{
					echo "<a href='profile.php?id=$_SESSION[id]'><div class='button' id='profile'>PROFILE</div></a>";
				}
				
			}
		?>
			
			<a href="creators.php"><div class="button<?php if($sec == 3) echo " active"; ?>" id="profile">CREATORS</div></a>
			<a href="creations.php"><div class="button<?php if($sec == 2) echo " active"; ?>" id="profile">CREATIONS</div></a>
			<a href="index.php"><div class="button<?php if($sec == 1) echo " active"; ?>" id="profile">HOME</div></a>

		</div>
		<div id="containerSearchTitle">
			<input type="text" id="search" placeholder="Find Creation / Creator here">
			<span id="searchBtn" class="icon icon-search"></span>
		</div>

		<div id="containerLoginBox">
			<form action="loginprocess.php" method="POST">
				<label>NIM</label>
				<span id="validator">Bukan merupakan NIM yang valid</span>
				<input type="text" name="nim" placeholder="Masukkan NIM yang valid" onblur="validate()" id="nimm"/>
				<label>Password</label>
				<input type="password" name="pass" placeholder="Password"/>
				<input type="checkbox" name="alwaysLogin" value="1" class="marginTop10 marginBottom10" style="display: inline; width: auto;"/> Biarkan saya tetap masuk

				<button class="submit marginAuto" type="submit"/>Login</button>
			</form>
		</div>
	</div>
</div>


