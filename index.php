

<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
<?php
include "include.php";
?>
</head>

<body>

<?php
include "header.php";
include "slide.php";
?>

<div id="containerBody">
	<article>
		
		<h1 class="borderBot">Newest Creations</h1>
		<div class="wrapperCreations4x">
			<div class="scrollNext"><span class="icon icon-chevron-right"></span></div>
			<div class="scrollPrev"><span class="icon icon-chevron-left"></span></div>
			<div class="containerCreations4x scrollable">
				
				<?php
				include "connect.php";
				$sql = "SELECT * FROM creation WHERE cr_publish = '1' ORDER BY cr_date DESC LIMIT 0,5";
				$result = mysqli_query($conn, $sql);
				while($row = mysqli_fetch_array($result)){
					$sql_user = "SELECT * FROM user WHERE user_id = '$row[cr_creators]'";
					$result_user = mysqli_query($conn, $sql_user);
					$row_user = mysqli_fetch_array($result_user);

					$sql_likes = "SELECT * FROM likes WHERE lk_creation = '$row[cr_id]'";
					$result_likes = mysqli_query($conn, $sql_likes);
					$likes = mysqli_num_rows($result_likes);

					$sql_comment = "SELECT * FROM comment WHERE com_creation = '$row[cr_id]'";
					$result_comment = mysqli_query($conn, $sql_comment);
					$countcomment = mysqli_num_rows($result_comment);

					$sql_rate = "SELECT * FROM rate WHERE rt_creation = '$row[cr_id]'";
					$result_rate = mysqli_query($conn, $sql_rate);
					$countrate = mysqli_num_rows($result_rate);		
					$rate = 0;
					if($countrate != 0){
						while($row_rate = mysqli_fetch_array($result_rate)){
							$rate += $row_rate['rt_score'];
						}
						$rate /= $countrate;
					}
					?>
						<a href="creation.php?id=<?php echo ($row['cr_id']) ?>">
						<div style="background-image: url('img/creation/<?php echo ($row['cr_id']) ?>.jpg')">
							<div class="info">
								<h1><?php echo ($row['cr_title']) ?></h1>
								<h2>by: <?php echo ($row_user['user_fullname']) ?></h2>

								<img class="thumbCreators" src="img/usr/<?php echo ($row_user['user_nim']) ?>.jpg"/>

								<div class="parameter">
									<span>
										<span class="icon icon-eye-open"></span> <?php echo ($row['cr_view']) ?> 
									</span>
									<span>
										<span class="icon icon-heart"></span> <?php echo ($likes) ?>  
									</span>
									<span>
										<span class="icon icon-comment"></span> <?php echo ($countcomment) ?>
									</span>
								</div>

								<div class="rate">
									<?php
										$printed = 0;
										for ($i = 1; $i < $rate; $i++){
											echo "<span class='icon icon-star'></span>";
											$printed++;
										}
										for ($i = 0; $i < 5 - $printed; $i++){
											echo "<span class='icon icon-star-empty'></span>";
										}
									?>
								</div>


							</div>
						</div>
					</a>
					<?php
				}

			?>

			</div>
		</div>


		<a href="creations.php"><div class="button more"><span class="icon icon-chevron-right"></span> See More</div></a>

		<hr>

		<h1 class="borderBot">Featured Creations</h1>

		<div class="containerCreationsBasicInfo">
			<?php
				$sql = "SELECT * FROM creation ORDER BY cr_view DESC";
				$result = mysqli_query($conn, $sql);
				$row = mysqli_fetch_array($result);

				$sql_cat = "SELECT * FROM category WHERE cat_id = '$row[cr_category]'";
				$result_cat = mysqli_query($conn, $sql_cat);
				$row_cat = mysqli_fetch_array($result_cat);

				$sql_subcat = "SELECT * FROM subcategory WHERE subcat_id = '$row[cr_subcategory]'";
				$result_subcat = mysqli_query($conn, $sql_subcat);
				$row_subcat = mysqli_fetch_array($result_subcat);

				$sql_creators = "SELECT * FROM user WHERE user_id = '$row[cr_creators]'";
				$result_creators = mysqli_query($conn, $sql_creators);
				$row_creators = mysqli_fetch_array($result_creators);
			?>
			<img src="img/creation/<?php echo $row['cr_id']?>.jpg"/>
			<div class="containerInfo">
				<h1><?php echo $row['cr_title']?></h1>
				<h2><?php echo $row_cat['cat_category']?></h2>
				<h3><?php echo $row_subcat['subcat_subcategory']?></h3>
				<div class="creators"><span class="marginRight10">Oleh : </span><img src="img/usr/<?php echo $row_creators['user_nim']?>.jpg"><span><?php echo $row_creators['user_fullname']?></span></div>
				
				<p><?php echo $row['cr_description']?></p>
			</div>
			<div class="readMoreCreations pointer"><span class="icon icon-chevron-right"></span><span class="text">Read More</span></div>
		</div>

		<hr>

		<h1 class="borderBot">Top Creators</h1>
		<div class="containerCreators4x">
			
			<?php
				include "connect.php";
				$sql = "SELECT * FROM user WHERE user_admin = 0 ORDER BY user_view DESC LIMIT 0,3";
				$result = mysqli_query($conn, $sql);
				while($row = mysqli_fetch_array($result)){
			?>
				<a href="profile.php?id=<?php echo ($row['user_id']) ?>">
					<div>
						<img src="img/usr/<?php echo ($row['user_nim']) ?>.jpg">
						<div class="readMoreCreations pointer"><span class="icon icon-chevron-right"></span><span class="text">See Profile</span></div>
						<div class="info">

							<h1><?php echo ($row['user_fullname']) ?></h1>
							<h2><?php echo ($row['user_title']) ?></h2>

							<p>
								<?php echo ($row['user_desc']) ?>
							</p>
						</div>
					</div>
				</a>
				
			<?php
				}
			?>

			



		</div>

		<a href="creators.php"><div class="button more"><span class="icon icon-chevron-right"></span> See More</div></a>

<!--
		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"/>
			<div class="deskripsi">
				<h1>Angry Birds</h1>
				Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone,
				namun kini telah dirilis di berbagai media.
				Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. 
				Bahkan pejabat Inggris pun tertarik dengan permainan ini.
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		
		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="https://upload.wikimedia.org/wikipedia/id/thumb/3/3a/Piala_Oscar.jpg/220px-Piala_Oscar.jpg"/>
			<div class="deskripsi">
				<h1>Piala Oscar</h1>
				Piala Oscar adalah penghargaan film Amerika untuk menghargai karya dalam industri film.
				Berbagai kategori pemenang diberikan penghargaan berupa sebuah salinan patung oleh Academy Award of Merit, yang lebih dikenal dengan julukan Oscar.
				Penghargaan ini pertama kali diselenggarakan pada tahun 1929 di Hotel Hollywood Roosevelt.
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="http://animalia-life.com/data_images/alaskan-malamute/alaskan-malamute1.jpg"/>
			<div class="deskripsi">
				<h1>Alaskan Malamut</h1>
			Malamut Alaska adalah salah satu ras anjing bertubuh besar yang berasal dari Alaska.
			Dulunya ras anjing ini dimanfaatkan untuk menarik kereta barang yang tahan terhadap suhu sangat dingin,
			namun sekarang anjing ini dirawat sebagai peliharaan keluarga dan anjing pertunjukan.	
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"/>
			<div class="deskripsi">
				<h1>Angry Birds</h1>
				Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone,
				namun kini telah dirilis di berbagai media.
				Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. 
				Bahkan pejabat Inggris pun tertarik dengan permainan ini.
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		<div class="containerProduct">
			<h2>14 februari 2015 by Gunawan Tibi</h2>
			
			<img src="http://icons.iconarchive.com/icons/femfoyou/angry-birds/1024/angry-bird-icon.png"/>
			<div class="deskripsi">
				<h1>Angry Birds</h1>
				Angry Birds adalah permainan video yang pada awalnya tersedia untuk aplikasi iPad dan iPhone,
				namun kini telah dirilis di berbagai media.
				Permainan ini membuat ketagihan penggunanya sehingga telah diunduh lebih dari 1 miliar pengguna. 
				Bahkan pejabat Inggris pun tertarik dengan permainan ini.
			</div>
			<a href="detail.php"><button>Detail</button></a>

		</div>

		<ul id="containerPage">
			<li id="1" class="active">1</li>
			<li id="2">2</li>
			<li id="3">3</li>
			<li id="4">4</li>
			<li id="5">5</li>
			<li id="5">></li>
		</ul>

-->

	</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>