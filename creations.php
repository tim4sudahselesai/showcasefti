<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
	<?php
		include "include.php"
	?>
</head>

<body>

<?php
include "header.php";
include "creationsheader.php";
?>

<div id="containerBody">
	<?php
	include "creationscategory.php";
	?>
	<!-- <nav id="submenu">
		<ul>
			<a href="showcase.php"><li>Aplikasi</li></a>
			<a href="showcase.php"><li class="active">Game</li></a>
			<a href="showcase.php"><li>Musik</li></a>
			<a href="showcase.php"><li>Video</li></a>
			<a href="showcase.php"><li>Lainnya</li></a>

		</ul>
	</nav> -->

	<article>
		
		<div class="containerCreations4x">

			<?php
				include "connect.php";
				$sql = "SELECT * FROM creation WHERE cr_publish = '1' ";
				switch ($cat) {
					case 1:
						$sql .= "AND cr_category = 1";
						break;
					case 2:
						$sql .= "AND cr_category = 2";
						break;
					case 3:
						$sql .= "AND cr_category = 3";
						break;
					case 4:
						$sql .= "AND cr_category = 4";
						break;
					case 5:
						$sql .= "AND cr_category = 5";
						break;
				}
				$sql .= " ORDER BY cr_date DESC";
				$result = mysqli_query($conn, $sql);
				while($row = mysqli_fetch_array($result)){
					$sql_user = "SELECT * FROM user WHERE user_id = '$row[cr_creators]'";
					$result_user = mysqli_query($conn, $sql_user);
					$row_user = mysqli_fetch_array($result_user);

					$sql_likes = "SELECT * FROM likes WHERE lk_creation = '$row[cr_id]'";
					$result_likes = mysqli_query($conn, $sql_likes);
					$likes = mysqli_num_rows($result_likes);

					$sql_comment = "SELECT * FROM comment WHERE com_creation = '$row[cr_id]'";
					$result_comment = mysqli_query($conn, $sql_comment);
					$countcomment = mysqli_num_rows($result_comment);

					$sql_rate = "SELECT * FROM rate WHERE rt_creation = '$row[cr_id]'";
					$result_rate = mysqli_query($conn, $sql_rate);
					$countrate = mysqli_num_rows($result_rate);		
					$rate = 0;
					if($countrate != 0){
						while($row_rate = mysqli_fetch_array($result_rate)){
							$rate += $row_rate['rt_score'];
						}
						$rate /= $countrate;
					}
					?>
						<a href="creation.php?id=<?php echo ($row['cr_id']) ?>">
						<div style="background-image: url('img/creation/<?php echo ($row['cr_id']) ?>.jpg')">
								<div class="info">

									
									<h1><?php echo ($row['cr_title']) ?></h1>
									<h2>by: <?php echo ($row_user['user_fullname']) ?></h2>

									<img class="thumbCreators" src="img/usr/<?php echo ($row_user['user_nim']) ?>.jpg"/>

									<div class="parameter">
										<span>
											<span class="icon icon-eye-open"></span> <?php echo ($row['cr_view']) ?> 
										</span>
										<span>
											<span class="icon icon-heart"></span> <?php echo ($likes) ?>  
										</span>
										<span>
											<span class="icon icon-comment"></span> <?php echo ($countcomment) ?>
										</span>
									</div>

									<div class="rate">
										<?php
											$printed = 0;
											for ($i = 1; $i < $rate; $i++){
												echo "<span class='icon icon-star'></span>";
												$printed++;
											}
											for ($i = 0; $i < 5 - $printed; $i++){
												echo "<span class='icon icon-star-empty'></span>";
											}
										?>
									</div>


								</div>
							</div>
							</a>
					<?php
				}

			?>

		</div>



	</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>