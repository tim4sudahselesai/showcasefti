<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
<?php
include "include.php";
?>
<meta property="og:url" content="http://vhost.ti.ukdw.ac.id/creation.php" />
<meta property="og:image" content="http://vhost.ti.ukdw.ac.id/img/cytus.png" />
<meta property="og:title" content="FTI Showcase : CYTUS" />
<meta property="og:description" content="Rhythm game developed by Rayark Games, an independent game developer in Taiwan" />
</head>

<body>

<?php
include "header.php";
include "profileheader.php";
?>

<div id="containerBody">

	<article>
	
		<h1>Description of Me</h1>
		<p class="profileDescription marginAuto">
			<?php echo $row['user_desc']?>
		</p>
		
		<h1>Biodata</h1>
		
		<table id="profileTableDataDiri">
			<tbody>
				<tr>
					<td>Fullname</td>
					<td><?php echo $row['user_fullname']?></td>
				</tr>
				<tr>
					<td>NIM</td>
					<td><?php echo $row['user_nim']?></td>
				</tr>
				<tr>
					<td>Place & Birth Date</td>
					<td><?php echo $row['user_tempat']?>, <?php echo $row['user_tanggal']?></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><?php echo $row['user_alamat']?></td>
				</tr>
				<tr>
					<td>Contact</td>
					<td><?php echo $row['user_telepon']?></td>
				</tr>
			</tbody>
		</table>
		
		<br/>
		<h1>Creations by <?php echo $row['user_fullname']?></h1>
		<br/>


			<div class="containerCreations4x">
				
				<?php
				include "connect.php";
				$sql = "SELECT * FROM creation WHERE cr_creators = '$_GET[id]' AND cr_publish='1' ORDER BY cr_date DESC";
				$result = mysqli_query($conn, $sql);
				while($row = mysqli_fetch_array($result)){
					$sql_user = "SELECT * FROM user WHERE user_id = '$row[cr_creators]'";
					$result_user = mysqli_query($conn, $sql_user);
					$row_user = mysqli_fetch_array($result_user);

					$sql_likes = "SELECT * FROM likes WHERE lk_creation = '$row[cr_id]'";
					$result_likes = mysqli_query($conn, $sql_likes);
					$likes = mysqli_num_rows($result_likes);

					$sql_comment = "SELECT * FROM comment WHERE com_creation = '$row[cr_id]'";
					$result_comment = mysqli_query($conn, $sql_comment);
					$countcomment = mysqli_num_rows($result_comment);

					$sql_rate = "SELECT * FROM rate WHERE rt_creation = '$row[cr_id]'";
					$result_rate = mysqli_query($conn, $sql_rate);
					$countrate = mysqli_num_rows($result_rate);		
					$rate = 0;
					if($countrate != 0){
						while($row_rate = mysqli_fetch_array($result_rate)){
							$rate += $row_rate['rt_score'];
						}
						$rate /= $countrate;
					}
					?>
						<a href="creation.php?id=<?php echo ($row['cr_id']) ?>">
						<div style="background-image: url('img/creation/<?php echo ($row['cr_id']) ?>.jpg')">
								<div class="info">

									
									<h1><?php echo ($row['cr_title']) ?></h1>
									<h2>by: <?php echo ($row_user['user_fullname']) ?></h2>

									<img class="thumbCreators" src="img/usr/<?php echo ($row_user['user_nim']) ?>.jpg"/>

									<div class="parameter">
										<span>
											<span class="icon icon-eye-open"></span> <?php echo ($row['cr_view']) ?> 
										</span>
										<span>
											<span class="icon icon-heart"></span> <?php echo ($likes) ?>  
										</span>
										<span>
											<span class="icon icon-comment"></span> <?php echo ($countcomment) ?>
										</span>
									</div>

									<div class="rate">
										<?php
											$printed = 0;
											for ($i = 1; $i < $rate; $i++){
												echo "<span class='icon icon-star'></span>";
												$printed++;
											}
											for ($i = 0; $i < 5 - $printed; $i++){
												echo "<span class='icon icon-star-empty'></span>";
											}
										?>
									</div>


								</div>
							</div>
							</a>
					<?php
				}

			?>
			<?php
				if ($_GET['id'] == $_SESSION['id']){
					?>
						<a href="upload.php?">
							<div style="background-image: url('img/plus.jpg')">
								<div class="info">
									<h1>Upload new Creation</h1>
								</div>
							</div>
						</a>
					<?php	
				}
			?>
			

		</div>

		
		</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>