<div id="containerHead" class="profile">
<div id="containerHeadOverlay">

	<?php
		$cat = 0;
		if (isset($_GET['category'])){
			switch ($_GET['category']) {
				case 'application':
					$cat = 1;
					break;
				case 'game':
					$cat = 2;
					break;
				case 'music':
					$cat = 3;
					break;
				case 'video':
					$cat = 4;
					break;
				case 'other':
					$cat = 5;
					break;
			}
		}

	?>

	<div id="wrapperCreationsCover" style="background-image: url('img/creations.jpg')">
	<?php
	if (isset($_SESSION['id'])) {
	?>
	<a href="upload.php">
		<div id="btnUpload">
			<span class="icon icon-plus"></span>
			<span>Upload Creation</span>
		</div>
	</a>
	<?php
	}
	?>
		<?php
			switch ($cat) {
				case 0:
					?>
					<h1>Creations</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo</p>
					<?php
					break;
				case 1:
					?>
					<h1>Application</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo</p>
					<?php
					break;
				case 2:
					?>
					<h1>Game</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo</p>
					<?php
					break;
				case 3:
					?>
					<h1>Music</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo</p>
					<?php
					break;
				case 4:
					?>
					<h1>Video</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo</p>
					<?php
					break;
				case 5:
					?>
					<h1>Other</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labo</p>
					<?php
					break;
				
				default:
					# code...
					break;
			}
		?>
	</div>

</div>
</div>
