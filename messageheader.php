<div id="containerHead" class="message" <?php if (isset($submsg)) echo "style='height: 245px'"?>>
<div id="containerHeadOverlay">

	<div id="wrapperMessageCover">
		<h1><?php echo $msg ?></h1>
		<?php
		if (isset($submsg)){
			echo "<p>$submsg</p>";
		}
		?>
	</div>

</div>
</div>
