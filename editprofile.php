<!DOCTYPE html>
<html>

<head>
<title>Showcase</title>
<?php
include "include.php";
?>
<meta property="og:url" content="http://vhost.ti.ukdw.ac.id/creation.php" />
<meta property="og:image" content="http://vhost.ti.ukdw.ac.id/img/cytus.png" />
<meta property="og:title" content="FTI Showcase : CYTUS" />
<meta property="og:description" content="Rhythm game developed by Rayark Games, an independent game developer in Taiwan" />
</head>

<body>

<?php
include "header.php";
include "editprofileheader.php";
?>

<div id="containerBody">

	<article>
	
		<?php
			include "connect.php";
			$sql = "SELECT * FROM user WHERE user_id = '$_SESSION[id]'";
			$result = mysqli_query($conn, $sql);
			$row = mysqli_fetch_array($result);

		?>
		<h1>Photo</h1>

		<form action="editprofileprocess.php" method="POST" enctype="multipart/form-data">

		<?php echo "<img id='editProfilePhoto' src='img/usr/$row[user_nim].jpg'/>" ?>
		<input type="file" name="photo"></input>
		
		<h1>Cover Photo</h1>
		<?php echo "<img id='editProfilePhoto' src='img/usr/cov/$row[user_nim].jpg'/>" ?>
		<input type="file" name="coverphoto"></input>

		<h1>Biodata</h1>
			 
		<table id="profileTableDataDiri">
			<tbody>
				<tr>
					<td>Fullname</td>
					<td><input type="text" name="fullname" value="<?php echo $row['user_fullname']?>"></input></td>
				</tr>
				<tr>
					<td>Title</td>
					<td><input type="text" name="title" value="<?php echo $row['user_title']?>"></input></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea name="desc"><?php echo $row['user_desc']?></textarea></td>
				</tr>
				<tr>
					<td>NIM</td>
					<td><input readonly="true" name="nim" type="text" value="<?php echo $row['user_nim']?>"></input></td>
				</tr>
				<tr>
					<td>Birth Place</td>
					<td><input type="text" name="place" value="<?php echo $row['user_tempat']?>"></input></td>
				</tr>
				<tr>
					<td>Birth Date</td>
					<td><input type="text" name="date" value="<?php echo $row['user_tanggal']?>"></input></td>
				</tr>
				<tr>
					<td>Address</td>
					<td><input type="text" name="address" value="<?php echo $row['user_alamat']?>"></input></td>
				</tr>
				<tr>
					<td>Contact</td>
					<td><input type="text" name="contact" value="<?php echo $row['user_telepon']?>"></input></td>
				</tr>
			</tbody>
		</table>
		
		<button type="submit" class="marginAuto">Submit</button>
		
		</form>
				
		</article>

</div>


<?php
include "footer.php"
?>

</body>
</html>